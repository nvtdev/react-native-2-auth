import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Firebase from 'firebase';
import { Header, Button, CardSection, Spinner } from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component {
  state = { loggedIn: null };

  componentWillMount() {
    Firebase.initializeApp({
      apiKey: 'AIzaSyDXeNNuShwdglrOUEMc-J763mBHCDruNcM',
      authDomain: 'authentication-6ad51.firebaseapp.com',
      databaseURL: 'https://authentication-6ad51.firebaseio.com',
      projectId: 'authentication-6ad51',
      storageBucket: 'authentication-6ad51.appspot.com',
      messagingSenderId: '542380490595'
    });

    Firebase.auth().onAuthStateChanged((user) => {
      if (user)
        this.setState({ loggedIn: true });
      else
        this.setState({ loggedIn: false });
    });
  }

  renderContent() {
    switch (this.state.loggedIn) {
      case true:
        return (
          <CardSection>
            <Button onPress={() => Firebase.auth().signOut()}>Log out</Button>
          </CardSection>
        );
      case false:
        return <LoginForm />;
      default:
        return (
          <CardSection>
            <Spinner size="large" />
          </CardSection>
        );
    }
  }

  render() {
    return (
      <View>
        <Header headerText="Authentication" />
        {this.renderContent()}
      </View>
    );
  }
};

export default App;
